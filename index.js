const http = require("http");
const port = 3000;
const server = http.createServer((req,res) => {
	if(req.url == '/login'){
		res.writeHead(200,{'Content-Type':'text/plain'})
		res.end('Login Page')
	}else{
		res.writeHead(404,{'Content-Type':'text/plain'})
		res.end('Error: Page not Found')
	}
})

server.listen(port);
console.log(`Server now accessible at localhost:${port}`);